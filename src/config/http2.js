'use strict'

import axios from 'axios'
import {Encrypt,Decrypt} from '../utils/crypto2';
const baseUrl='http://vapi.pipe-welding.cn/api/';
axios.interceptors.response.use(response => {
    return response
}, error => {
    return Promise.resolve(error.response)
})

function checkStatus (response) {
    // loading
    // 如果http状态码正常，则直接返回数据
    if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
        if (response.data.code!=0)
        {
            if (response.data.message.indexOf('登录过期')!=-1)
            {

                //  return
            }
            return {
                data:{
                    success:false,
                    error_msg: response.data.message
                }
            }
        }
        else {

        }
        if (response.data.result)
        {
            response.data.result = JSON.parse(Decrypt(response.data.result))
        }
        return response.data
        // 如果不需要除了data之外的数据，可以直接 return response.data
    }
    // 异常状态下，把错误信息返回去
    return {
        status: -404,
        msg: '网络异常'
    }
}

function checkCode (res) {
    // 如果code异常(这里已经包括网络错误，服务器错误，后端抛出的错误)，可以弹出一个错误提示，告诉用户
    if (res.status === -404) {

    }
    if (res.data && (!res.data.success)) {
        // alert(res.data.error_msg)

    }
    return res
}
export const post2=function (url, data) {
    let sortPram;
    let setHeader={}
    /* if(data){*/
    data.timeStamp=new Date().getTime();
    // header.sign = md5(sortPram).toUpperCase()
    let enData = Encrypt(JSON.stringify(data))
    data = {data:enData};
    //  data.timeStamp= new Date().getTime();
    if (url.substring(0,5)!='/open')
    {
        // setHeader.token =Local.get('accessToken')
    }
    // header.token = "dd47d6958a54b0cd49eb5b19a3084c66"
    /* }else{
         console.log('32323')
         data = {timeStamp:new Date().getTime()}
     }*/
    return axios({
        method: 'post',
        url:baseUrl+url,
        data:data,
        timeout: 10000,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json;charset=UTF-8',
        }
    }).then(
        (response) => {
            return checkStatus(response)
        }
    ).then(
        (res) => {
            return checkCode(res)
        }
    )
}
export const get=function (url, params) {
    return axios({
        method: 'get',
        url:baseUrl+url,
        params, // get 请求时带的参数
        timeout: 10000,
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        }
    }).then(
        (response) => {
            return checkStatus(response)
        }
    ).then(
        (res) => {
            return checkCode(res)
        }
    )
}
export const getBase64=function (url, params) {
    let decodedData = window.btoa(unescape(encodeURIComponent(params.url)));
    return axios({
        method: 'get',
        url:baseUrl+url+decodedData+'.data',
        timeout: 10000,
        responseType:'arraybuffer',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'responseType':'arraybuffer'
        }
    }).then(
        (response) => {
            console.log(response,'0er09er')
            if (response&&response.data){
                response.data=get_image(response.data)
            }
            return response
        }
    ).then(
        (res) => {
            return res
        }
    )
}
function get_image(buffer){
    var binary = '';
    var url=''
    var bytes = new Uint8Array(buffer);
    for (var len = bytes.byteLength, i = 2; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    url="data:image/png;base64,"+window.btoa(binary);
    return url;
}