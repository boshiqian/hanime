import Vue from 'vue';
import Router from 'vue-router';
import {Local} from "../utils/common";

Vue.use(Router);

const routes = [
  {
    path: '*',
    redirect: '/home'
  },
  {
    name: 'home',
    component: () => import('../page/home/index'),
    meta: {
      title: '首页'
    }
  },
  {
    name: 'watch',
    component: () => import('../page/watch/index'),
    meta: {
      title: '视频'
    }
  }
];

// add route path
routes.forEach(route => {
  route.path = route.path || '/' + (route.name || '');
});

const router = new Router({ routes });

router.beforeEach((to, from, next) => {
  const title = to.meta && to.meta.title;
  if (title) {
    document.title = title;
  }
  if (to.path==='/login')
  {
    next();
  }
  else {
    if (to.meta.requiresAuth &&!Local.get('accessToken'))
    {
      next({ path: '/login' })
    }
    else {
      next()
    }
  }
});

export {
  router
};
