'use strict'

import axios from 'axios'
import md5 from 'js-md5';
import {Encrypt,Decrypt} from '../utils/crypto';
const APPID = "A8A1BC5DF5227318E4DFBF6C8D2E6E53";
import {Local} from "../utils/common";
import {logout} from "../utils/common";
import {baseUrl} from "./env";
import {Toast} from "vant";
axios.interceptors.request.use(config => {
    // loading
    config.baseURL=baseUrl
    return config
}, error => {
    return Promise.reject(error)
})

axios.interceptors.response.use(response => {
    return response
}, error => {
    return Promise.resolve(error.response)
})

function checkStatus (response) {
    // loading
    // 如果http状态码正常，则直接返回数据
    Toast.clear('all')
    if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
        if (response.data.code!=0)
        {
            if (response.data.message.indexOf('登录过期')!=-1)
            {
              //  return
            }
            return {
                data:{
                    success:false,
                    error_msg: response.data.message
                }
            }
        }
        else {
        }
        if (response.data.result)
        {
            response.data.result = JSON.parse(Decrypt(response.data.result))
        }
        return response.data
        // 如果不需要除了data之外的数据，可以直接 return response.data
    }
    // 异常状态下，把错误信息返回去
    return {
        status: -404,
        msg: '网络异常'
    }
}

function checkCode (res) {
    // 如果code异常(这里已经包括网络错误，服务器错误，后端抛出的错误)，可以弹出一个错误提示，告诉用户
    if (res.status === -404) {
        Toast.fail({duration:2000,message:res.msg});
    }
    if (res.data && (!res.data.success)) {
       // alert(res.data.error_msg)
        Toast.fail({duration:2000,message:res.data.error_msg})
    }
    return res
}
export const post=function (url, data) {
        let enData = Encrypt(JSON.stringify(data))
        data = {data:enData,timeStamp:new Date().getTime()};
    return axios({
        method: 'post',
        url,
        data:data,
        timeout: 10000,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json;charset=UTF-8',
            'appid':APPID
        }
    }).then(
        (response) => {
            return checkStatus(response)
        }
    ).then(
        (res) => {
            return checkCode(res)
        }
    )
}
export const get=function (url, params) {
    let setHeader={}
    if (url.substring(0,5)!='/open')
    {
        setHeader.token =Local.get('accessToken')
    }
    return axios({
        method: 'get',
        url,
        params, // get 请求时带的参数
        timeout: 10000,
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        }
    }).then(
        (response) => {
            return checkStatus(response)
        }
    ).then(
        (res) => {
            return checkCode(res)
        }
    )
}
export const getBase64=function (url, params) {
    let decodedData = window.btoa(unescape(encodeURIComponent(params.url)));
    return axios({
        method: 'get',
        url:url+decodedData+'.data',
        timeout: 10000,
        responseType:'arraybuffer',
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        }
    }).then(
        (response) => {
            if (response&&response.data){
                response.data=get_image(response.data)
            }
            return response
        }
    ).then(
        (res) => {
            return res
        }
    )
}
//参数排序
function objKeySort(arys) {
    //先用Object内置类的keys方法获取要排序对象的属性名数组，再利用Array的sort方法进行排序
    let newObj = "";
    if (arys === null) {
        return;
    }

    const newkey = Object.keys(arys).sort();
    for (let i = 0; i < newkey.length; i++) {
        const newArrKey = newkey[i];
        newObj += newArrKey + "=" + arys[newArrKey] + "&";
    }

    return newObj.substring(0, newObj.length - 1);
}
function get_image(buffer){
    var binary = '';
    var url=''
    var bytes = new Uint8Array(buffer);
    for (var len = bytes.byteLength, i = 2; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    url="data:image/png;base64,"+window.btoa(binary);
    return url;
}
