import CryptoJS from 'crypto-js';
import md5 from 'js-md5';
import axios from 'axios'
import {baseUrl,dataSources} from './env';
import datas from '../data/data';
import {Encrypt} from '../utils/crypto';
const APPID = "218BD21F6F6F27B21CC78F344A3CEAC8"
const service =axios.create({
//  baseURL:'api/', // api 的 base_url
  baseURL:baseUrl, // api 的 base_url
  timeout: 5000, // request timeout
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    },
});


const servicef =function(parameter){
    console.log(parameter,'4646gf')
  if(dataSources=='local'){
    //定义回调函数和axios一致
    const promist = new Promise(function(resolve,reject){
        var data=datas[parameter.url];
        if(typeof data=='string'){
          data= JSON.parse(data);
        }
        resolve(data);
    })
    return promist;
  }
  return service(parameter);
}


  service.interceptors.request.use(
    config => {
      // Do something before request is sent
    //   if (store.getters.token) {
    //     // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
    //     config.headers['X-Token'] = getToken()
    //   }
        let params=config.params.data;
        console.log(params,'7979746464',config)
        if(config.params.data){
            config.params.data.timeStamp= new Date().getTime();
            let sortPram = objKeySort(config.params.data) + APPID
            console.log(md5,'md5')
            config.headers.sign = md5(sortPram).toUpperCase()

            let enData = Encrypt(JSON.stringify(config.params.data))


         //   config.params.data = {data:enData};
          //  config.params.data.timeStamp= new Date().getTime();
           // config.headers.token = "dd47d6958a54b0cd49eb5b19a3084c66"
        }else{
            config.params.data = {timeStamp:new Date().getTime()}
        }
        config.params.data=''
        console.log(config,'config888');
      return config
    },
    error => {
      // Do something with request error
      console.log(error) // for debug
      Promise.reject(error)
    }
  )

  // response interceptor
service.interceptors.response.use(
    //response => response,
    /**
     * 下面的注释为通过在response里，自定义code来标示请求状态
     * 当code返回如下情况则说明权限有问题，登出并返回到登录页
     * 如想通过 xmlhttprequest 来状态码标识 逻辑可写在下面error中
     * 以下代码均为样例，请结合自生需求加以修改，若不需要，则可删除
     */
    response => {
      const res = response.data;
      if (res.ResultCode !== 200) {
        // Message({
        //   message: res.message,
        //   type: 'error',
        //   duration: 5 * 1000
        // })
        // // 50008:非法的token; 50012:其他客户端登录了;  50014:Token 过期了;
        // if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        //   // 请自行在引入 MessageBox
        //   // import { Message, MessageBox } from 'element-ui'
        //   MessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
        //     confirmButtonText: '重新登录',
        //     cancelButtonText: '取消',
        //     type: 'warning'
        //   }).then(() => {
        //     store.dispatch('FedLogOut').then(() => {
        //       location.reload() // 为了重新实例化vue-router对象 避免bug
        //     })
        //   })
        // }
        console.log(1);
        return Promise.reject('error')
      } else {
        if(typeof response.data.Tag=='string'){
          return JSON.parse(response.data.Tag);
        }else{
          return response.data.Tag;
        }
      }
    },
    error => {
      
      return Promise.reject(error)
    }
  )
//参数排序
function objKeySort(arys) {
    //先用Object内置类的keys方法获取要排序对象的属性名数组，再利用Array的sort方法进行排序
    let newObj = "";
    if (arys === null) {
        return;
    }

    const newkey = Object.keys(arys).sort();
    for (let i = 0; i < newkey.length; i++) {
        const newArrKey = newkey[i];
        newObj += newArrKey + "=" + arys[newArrKey] + "&";
    }

    return newObj.substring(0, newObj.length - 1);
}

  export default servicef