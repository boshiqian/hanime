import { createRouter, createWebHashHistory } from 'vue-router'
import {Toast} from "vant";

const router = createRouter({
  history: createWebHashHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/views/Home.vue'),
      meta: {
        index: 1
      }
    },
    {
      path: '/watch/:id',
      name: 'watch',
      component: () => import('@/views/watch/index.vue'),
      meta: {
        index: 1
      }
    },
    {
      path: '/category/:id',
      name: 'category',
      component: () => import('@/views/category/index.vue'),
    },
    {
      path: '/search',
      name: 'search',
      component: () => import('@/views/search/index.vue'),
    },
    {
      path: '/searchVideo/:key',
      name: 'searchVideo',
      component: () => import( '@/views/search/index.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login/index.vue'),
    },
    {
      path: '/member',
      name: 'member',
      component: () => import('@/views/member/index'),
    },
  ]
})
router.beforeEach((to, from, next) => {
  const title = to.meta && to.meta.title;
  Toast.loading({
    duration: 0,
    message: 'Loading...',
    forbidClick: true,
  });
  if (title) {
    document.title = title;
  }
  if (to.path==='/login')
  {
    next();
  }
  else {
    if (to.meta.requiresAuth &&!Local.get('accessToken'))
    {
      next({ path: '/login' })
    }
    else {
      next()
    }
  }
});
router.afterEach((to,from,next)=>{

})

export default router
