import defaultImg from '@/assets/images/notfound.png'
import {getCryptImg} from "../api/page";

const defineDirective=(app)=>{
    app.directive('lazyload',{
        mounted(el,binding){
            el.src =defaultImg
            const observer = new IntersectionObserver(([{ isIntersecting }]) => {
                //如果被监听的DOM元素进入了可视区
                if (isIntersecting) {
                    //取消监听
                    observer.unobserve(el)
                    //给DOM绑定一个事件,如果传过来的图片是个错误的地址,就把默认的图片给src赋值
                    el.onerror = () => {
                        el.src =''
                    }
                    //给图片赋值
                    getCryptImg({url:binding.value}).then((res)=>{
                        if (res&&res.data)
                        {
                            el.src=res.data
                        }
                        else {
                        }
                    })
                }
            }, {
                threshold: 0.01
            })
            //监听DOM元素
            observer.observe(el)
        }
    })
}
export default {
    install(app){
        defineDirective(app)
    }
}
