/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除我方将保留所有法律责任追究！
 * 本系统已申请软件著作权，受国家版权局知识产权以及国家计算机软件著作权保护！
 * 可正常分享和学习源码，不得用于违法犯罪活动，违者必究！
 * Copyright (c) 2020 陈尼克 all rights reserved.
 * 版权所有，侵权必究！
 */

import {GetUserInfo} from "../api/user";
import {getCategory, getConfig} from "../api/page";
import {Local} from "../utils/common";

export default {
  async updateUserInfo(ctx) {
     GetUserInfo({userId:Local.get('accessToken'),isUpdate:true}).then((res)=>{
       ctx.commit('setUserInfo', {
         userInfo: res.result||{}
       })
     })
  },
    async updateConfig(ctx) {
        getConfig({}).then((res)=>{
            ctx.commit('setConfig', {
                config: res.result.config||{}
            })
        })
    },
    async getCategory(ctx) {
        getCategory({}).then((res)=>{
            ctx.commit('setCategory', {
                category: res.result||[]
            })
        })
    }
}

