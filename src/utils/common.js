import {LoginPrams,GetUserInfo,Login} from "../api/user";
export const Local = {
    // 设置永久缓存
    set(key, val) {
        window.localStorage.setItem(key, JSON.stringify(val));
    },
    // 获取永久缓存
    get(key) {
        let json = window.localStorage.getItem(key);
        return JSON.parse(json);
    },
    // 移除永久缓存
    remove(key) {
        window.localStorage.removeItem(key);
    },
    // 移除全部永久缓存
    clear() {
        window.localStorage.clear();
    },
}
export const logout = function () {
    Local.clear()
   /* _this.$router.push('/login')
    _this.$store.commit('modifyUserInfo',{
        moneyBalance:0,
        nickName:''
    })*/
}
export const getExactTime=function(time) {
    var date = new Date(time);
    // var date = new Date(time* 1000);
    var year = date.getFullYear() + '-';
    var month = (date.getMonth()+1 < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) + '-';
    var dates = date.getDate() + ' ';
    var hour = date.getHours() + ':';
    var min = date.getMinutes() + ':';
    var second = date.getSeconds();
    return year + month + dates + hour + min + second ;
}
window.loginByParams=function (data) {
    Local.clear();
    LoginPrams(data).then(res=>{
        if (res.code==0)         {
            Local.set('accessToken',res.result.lastLoginToken)
           /* _this.$router.push('/card')
            GetUserInfo().then(res=>{
                if (res.code==0)
                {
                    _this.$store.commit('modifyUserInfo',res.result)
                }
            });*/
        }
    })
}
export const formatDate=function (time) {
   return  time?time.replace(/([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])/,''):'--'
}
