import {post} from "../config/http";
export function Login(data){//登录
  return post(
   '/user/loginByPhone',
    data
  )
}
export function GetWithdrawCode(){//提现验证码
  return post(
      '/user/sendCode',
      {}
  )
}
export function GetLoginCode(data){//手机登录验证码
  return post(
      '/user/sendCode',
      data
  )
}
export function GetRegisterCode() {//注册验证码
  return post(
      '/open/user/sendCodeByReg',
      {}
  )
}
export function Register(data){//用户注册
  return post(
      '/user/regUser',
      data
  )
}
export function GetUserInfo(data){//获取用户信息
  return post(
      '/user/getUserInfo',
      data
  )
}
export function updateUserInfo(data){//更新资料
  return post(
      '/user/updateUserInfo',
      data
  )
}

export function getCollect(data){//获取收藏视频
  return post(
      '/user/getUserFav',
      data
  )
}

export function updatetCollect(data){//更新收藏视频
  return post(
      '/user/updateUserFav',
      data
  )
}

