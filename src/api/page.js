import {post,get,getBase64} from "../config/http";
export function getBanner(){//获取banner图
    return post(
        '/getBanner',
        {}
    )
}
export function getCategory(){//获取分类
    return post(
        '/getCategory',
        {}
    )
}
export function getHomeVideo(data){//获取首页视频列表
    return post(
        '/listByIndexCol',
        data
    )
}
export function getHomeClass(data){//获取首页栏目
    return post(
        '/getIndexCol',
        data
    )
}
export function getVideoInfo(data){//视频详情
    return post(
        '/getVideoInfo',
        data
    )
}
export function getRelate(data){//相关视频列表
    return post(
        '/getByRandom',
        data
    )
}
export function searchVideo(data){//搜索视频
    return post(
        '/searchVideo',
        data
    )
}
export function getConfig(data){//搜索视频
    return post(
        '/getConfig',
        data
    )
}
export function getCoinItem(){//获取钻石商品
    return post(
        '/getCoinItem',
        {}
    )
}
export function getVipItem(){//获取VIP商品
    return post(
        '/getVipItem',
        {}
    )
}

export function getChannelList(){//获取支付通道
    return post(
        '/getChannelList',
        {}
    )
}

export function payOrder(data){//支付下单
    return post(
        '/pay/toPay',
        data
    )
}
export function orderVideo(data){//购买视频
    return post(
        'user/coinPay',
        data
    )
}
export function getCryptImg(data){//获取加密图片
    return getBase64(
        '/img/',
        data
    )
}
export function getADVList(data){//获取广告
    return post(
        '/getAdvList',
        data
    )
}
export function getListByCategory(data){//获取分类视频
    console.log(data,'classdat')
    return post(
        '/getListByCategory',
        data
    )
}
