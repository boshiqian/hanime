import {post2,get,getBase64} from "../config/http2";
export function GetVideoCategory(){//获取视频类目
    return post2(
        '/getVideoCategory',
        {}
    )
}
export function GetImg(data){//获取图片
    return getBase64(
        'img/',
        data
    )
}